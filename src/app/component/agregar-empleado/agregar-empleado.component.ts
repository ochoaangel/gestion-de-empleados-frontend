import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, MinLengthValidator, Validators } from '@angular/forms';
import { ApiCallService } from 'src/app/services/api-call.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
moment.locale('es')

@Component({
  selector: 'app-agregar-empleado',
  templateUrl: './agregar-empleado.component.html',
  styleUrls: ['./agregar-empleado.component.css']
})

export class AgregarEmpleadoComponent implements OnInit {
  YearsSelect: any[] = [];

  model: NgbDateStruct = {
    "year": 2021,
    "month": 4,
    "day": 17
  };


  allDivisions = [];
  allOffices = [];
  allPositions = [];

  constructor(
    private formBuilder: FormBuilder,
    private apiCall: ApiCallService
  ) { }

  userForm = this.formBuilder.group({
    Year: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
    Grade: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]],
    Month: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]],
    Office: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(3)]],
    Division: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    Position: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
    BirthDay: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
    BeginDate: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(10)]],
    BaseSalary: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
    Commission: ['', [Validators.maxLength(20)]],
    Contributions: ['', [Validators.maxLength(10)]],
    EmployeeName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    ProductionBonus: ['', [Validators.maxLength(20)]],
    Identification: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    EmployeeSurname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    CompensationBonus: ['', [Validators.maxLength(10)]],
  });

  ngOnInit(): void {

    this.YearsSelect = [];

    for (let index = 2020; index > 1990; index--) {
      this.YearsSelect.push(index);
    }

    this.apiCall.getDivisions().subscribe(resp => {
      this.allDivisions = resp;
    })

    this.apiCall.getOffices().subscribe(resp => {
      this.allOffices = resp;
    })

    this.apiCall.getPositions().subscribe(resp => {
      this.allPositions = resp;
    })

  }

  get YearInvalid() { return this.userForm.get('Year')?.invalid && this.userForm.get('Year')?.touched }
  get MonthInvalid() { return this.userForm.get('Month')?.invalid && this.userForm.get('Month')?.touched }
  get GradeInvalid() { return this.userForm.get('Grade')?.invalid && this.userForm.get('Grade')?.touched }
  get OfficeInvalid() { return this.userForm.get('Office')?.invalid && this.userForm.get('Office')?.touched }
  get DivisionInvalid() { return this.userForm.get('Division')?.invalid && this.userForm.get('Division')?.touched }
  get PositionInvalid() { return this.userForm.get('Position')?.invalid && this.userForm.get('Position')?.touched }
  get BirthDayInvalid() { return this.userForm.get('BirthDay')?.invalid && this.userForm.get('BirthDay')?.touched }
  get BeginDateInvalid() { return this.userForm.get('BeginDate')?.invalid && this.userForm.get('BeginDate')?.touched }
  get BaseSalaryInvalid() { return this.userForm.get('BaseSalary')?.invalid && this.userForm.get('BaseSalary')?.touched }
  get CommissionInvalid() { return this.userForm.get('Commission')?.invalid && this.userForm.get('Commission')?.touched }
  get EmployeeCodeInvalid() { return this.userForm.get('EmployeeCode')?.invalid && this.userForm.get('EmployeeCode')?.touched }
  get EmployeeNameInvalid() { return this.userForm.get('EmployeeName')?.invalid && this.userForm.get('EmployeeName')?.touched }
  get ContributionsInvalid() { return this.userForm.get('Contributions')?.invalid && this.userForm.get('Contributions')?.touched }
  get IdentificationInvalid() { return this.userForm.get('Identification')?.invalid && this.userForm.get('Identification')?.touched }
  get EmployeeSurnameInvalid() { return this.userForm.get('EmployeeSurname')?.invalid && this.userForm.get('EmployeeSurname')?.touched }
  get ProductionBonusInvalid() { return this.userForm.get('ProductionBonus')?.invalid && this.userForm.get('ProductionBonus')?.touched }
  get CompensationBonusInvalid() { return this.userForm.get('CompensationBonus')?.invalid && this.userForm.get('CompensationBonus')?.touched }


  submit() {
    if (this.userForm.invalid) {
      Object.values(this.userForm.controls).forEach(control => { control.markAsTouched() })

    } else {
      let end = this.userForm.value;
      let BeD = this.userForm.value['BeginDate'];
      let BiD = this.userForm.value['BirthDay'];
      end.BeginDate = moment(`${BeD.year} ${BeD.month} ${BeD.day}`, 'YYYY MM DD').format('MMM DD, YYYY');
      end.Birthday = moment(`${BiD.year} ${BiD.month} ${BiD.day}`, 'YYYY MM DD').format('MMM DD, YYYY');
      end.Commission = end['Commission'] || 0;
      end.Contributions = end['Contributions'] || 0;
      end.CompensationBonus = end['CompensationBonus'] || 0;
      end.EmployeeCode = this.getRandomInt(10001009, 19999999);
      end.ProductionBonus = end['ProductionBonus'] || 0;
      delete end.BirthDay;
      console.log('depues', end)
      this.apiCall.setEmployee(end).subscribe((resp) => {
        console.log('Listo.   ya subió..')
        Swal.fire({
          icon: 'success',
          title: `Employee added`,
        })

        this.userForm.patchValue({
          Year: '',
          Grade: '',
          Month: '',
          Office: '',
          Division: '',
          Position: '',
          BirthDay: '',
          BeginDate: '',
          BaseSalary: '',
          Commission: '',
          Contributions: '',
          EmployeeName: '',
          ProductionBonus: '',
          Identification: '',
          EmployeeSurname: '',
          CompensationBonus: ''
        })
        Object.values(this.userForm.controls).forEach(control => { control.markAsUntouched(); })

      })
    }
  }

  SelectPositionChanged(e: any) {
    console.log(e)
    this.userForm.controls['Position'].setValue(e.target.value, { onlySelf: true })
    console.log(this.userForm.value)
  }

  getRandomInt(min: any, max: any) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}


