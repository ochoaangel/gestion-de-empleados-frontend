import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Employee } from 'src/app/interfaces/interface';
import { UtilxService } from 'src/app/services/utilx.service';
import { ApiCallService } from 'src/app/services/api-call.service';

@Component({
  selector: 'app-promedio-salarios',
  templateUrl: './promedio-salarios.component.html',
  styleUrls: ['./promedio-salarios.component.css']
})
export class PromedioSalariosComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private apiCall: ApiCallService,
    private utilx: UtilxService
  ) { }

  employees: Employee[] = [];
  searched = false;
  TotalAverageSalary = 0;

  userForm = this.formBuilder.group({
    Code: ['', [Validators.required]]
  });

  ngOnInit(): void { }

  submit() {
    this.apiCall.getEmployeeSalary(this.userForm.value).subscribe(resp => {
      this.employees = this.utilx.getConsecutiveMonths(resp).map((employee: Employee) => {
        employee.TotalSalary = this.utilx.calculateTotalSalary(employee);
        return employee
      })
      this.TotalAverageSalary = 0;
      this.employees.forEach((employee: any) => {
        this.TotalAverageSalary = this.TotalAverageSalary + employee.TotalSalary;
      });
      console.log(this.employees)
      this.TotalAverageSalary = this.TotalAverageSalary / 3;
      this.searched = true;
    })
  }
}
