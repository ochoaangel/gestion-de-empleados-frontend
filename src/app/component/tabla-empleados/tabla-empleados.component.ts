import { Component, OnInit } from '@angular/core';
import { ApiCallService } from 'src/app/services/api-call.service';
import { Employee } from './../../interfaces/interface';
import * as moment from 'moment';
import { UtilxService } from 'src/app/services/utilx.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
moment.locale('es')
@Component({
  selector: 'app-tabla-empleados',
  templateUrl: './tabla-empleados.component.html',
  styleUrls: ['./tabla-empleados.component.css']
})
export class TablaEmpleadosComponent implements OnInit {

  page = 1;
  pageSize = 10;
  collectionSize = 0;
  employees: Employee[] = [];
  employeesAll: Employee[] = [];
  //////////////////////////
  employeeSelectedItem: any = {};
  //////////////////////////
  pageOne = 1;
  pageSizeOne = 5;
  employeesOne: Employee[] = [];
  employeesAllOne: Employee[] = [];
  collectionSizeOne = 0;
  //////////////////////////
  pageTwo = 1;
  pageSizeTwo = 5;
  employeesTwo: Employee[] = [];
  employeesAllTwo: Employee[] = [];
  collectionSizeTwo = 0;
  //////////////////////////
  pageThree = 1;
  pageSizeThree = 5;
  employeesThree: Employee[] = [];
  employeesAllThree: Employee[] = [];
  collectionSizeThree = 0;


  constructor(private apiCall: ApiCallService, private utilx: UtilxService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.apiCall.getEmployees().subscribe((employees) => {
      this.employees = this.prepareData(employees);
      this.employeesAll = employees;
      this.collectionSize = employees.length;
      this.refreshEmployees()
    })
  }

  refreshEmployees(target?: any) {
    switch (target) {
      case 'one':
        this.employeesOne = this.employeesAllOne.slice((this.pageOne - 1) * this.pageSizeOne, (this.pageOne - 1) * this.pageSizeOne + this.pageSizeOne);
        break;
      case 'two':
        this.employeesTwo = this.employeesAllTwo.slice((this.pageTwo - 1) * this.pageSizeTwo, (this.pageTwo - 1) * this.pageSizeTwo + this.pageSizeTwo);
        break;
      case 'three':
        this.employeesThree = this.employeesAllThree.slice((this.pageThree - 1) * this.pageSizeThree, (this.pageThree - 1) * this.pageSizeThree + this.pageSizeThree);
        break;
      default:
        this.employees = this.employeesAll.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
        break;
    }
  }

  prepareData(employees: Employee[]) {
    return employees.map(e => {
      e.TotalSalary = this.utilx.calculateTotalSalary(e);
      return e
    })
  }

  officeAndGrade() {
    console.log('BTN officeAndGrade  Pressed..')
  }

  allOfficeAndGrade() {
    console.log('BTN allOfficeAndGrade  Pressed..')
    console.log(this.employeesAll)
    this.employeesAll = this.utilx.sortByNumber(this.employeesAll, 'Grade').reverse();
    this.refreshEmployees();
  }

  positionAndGrade() {
    console.log('BTN positionAndGrade  Pressed..')
  }

  allPositionAndGrade() {
    console.log('BTN allPositionAndGrade  Pressed..')
  }


  openXl(content: any) {
    this.modalService.open(content, { size: 'xl' });
  }

  employeeSelected(employee: Employee, content: any) {
    
    this.employeesAllOne = this.employeesAll.filter((x: Employee) =>
      x.Office === employee.Office &&
      x.Grade === employee.Grade &&
      x.EmployeeName !== employee.EmployeeName &&
      x.EmployeeSurname !== employee.EmployeeSurname)

    this.employeesAllTwo = this.employeesAll.filter((x: Employee) =>
      x.Grade === employee.Grade &&
      x.EmployeeName !== employee.EmployeeName &&
      x.EmployeeSurname !== employee.EmployeeSurname)

    this.employeesAllThree = this.employeesAll.filter((x: Employee) =>
      x.Grade === employee.Grade &&
      x.Position === employee.Position &&
      x.EmployeeName !== employee.EmployeeName &&
      x.EmployeeSurname !== employee.EmployeeSurname)

    this.employeeSelectedItem = employee;
    this.refreshEmployees('one')
    this.refreshEmployees('two')
    this.refreshEmployees('three')
    this.modalService.open(content, { size: 'xl' });
  }
}
