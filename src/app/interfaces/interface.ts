export interface Employee {
    Id: number;
    Year: number;
    Month: number;
    Office: string;
    EmployeeCode: string;
    EmployeeName: string;
    EmployeeSurname: string;
    Division: string;
    Position: string;
    Grade: number;
    BeginDate: string;
    Birthday: string;
    Identification: string;
    BaseSalary: number;
    ProductionBonus: number;
    CompensationBonus: number;
    Commission: number;
    Contributions: number;
    TotalSalary?: number;
    fecha?: string;
}

export interface Office {
    Id: number;
    Name: string;
    Code: string;
    Description: string;
}

export interface Division {
    Id: number;
    Name: string;
    Code: string;
    Description: string;
}

export interface Position {
    Id: number;
    Name: string;
    Code: string;
    Description: string;
}