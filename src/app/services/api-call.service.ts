import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:3000';
  }

  setEmployee(body: any): Observable<any> {
    const url = '/setEmployee';
    return this.http.post<any>(this.baseUrl + url, body);
  }

  getEmployees(): Observable<any> {
    const url = '/getEmployees';
    return this.http.get<any>(this.baseUrl + url);
  }

  getEmployeeSalary(myheaders: any): Observable<any> {
    const url = '/getEmployeeSalary';
    let headers = new HttpHeaders().set('Code', '' + myheaders.Code)
    return this.http.get<any>(this.baseUrl + url, { headers });
  }

  getDivisions(): Observable<any> {
    const url = '/getDivisions';
    return this.http.get<any>(this.baseUrl + url);
  }

  getOffices(): Observable<any> {
    const url = '/getOffices';
    return this.http.get<any>(this.baseUrl + url);
  }

  getPositions(): Observable<any> {
    const url = '/getPositions';
    return this.http.get<any>(this.baseUrl + url);
  }
}
