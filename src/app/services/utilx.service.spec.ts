import { inject, TestBed } from '@angular/core/testing';

import { UtilxService } from './utilx.service';

describe('Pruebas útiles del servicio utilx..', () => {
    let service: UtilxService;

    it('Verificar si existe el servicio', inject([UtilxService], (service: UtilxService) => {
        expect(service).toBeTruthy();
    }));

    it('Creada una instancia de utilxService', () => {
        const instancex = new UtilxService();
        expect(instancex).toBeTruthy();
    });

});
