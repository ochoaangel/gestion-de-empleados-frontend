import { Injectable } from '@angular/core';
import { Employee } from '../interfaces/interface';
import * as moment from 'moment'


@Injectable({
  providedIn: 'root'
})
export class UtilxService {

  constructor() { }

  calculateTotalSalary(employee: Employee) {
    return employee.BaseSalary + employee.ProductionBonus +
      (employee.CompensationBonus * 0.75) +
      ((employee.BaseSalary + employee.CompensationBonus) * 0.08 +
        employee.Commission) - employee.Contributions
  }


  sortByString(list: any, key: any) {
    return list.sort((a: any, b: any) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0))
  }

  sortByNumber(list: any, key: any) {
    return list.sort((a: any, b: any) => {
      if (a[key] > b[key]) {
        return 1;
      } else if (a[key] < b[key]) {
        return -1;
      } else {
        return 0;
      }
    })
  }


  getConsecutiveMonths(data: any) {
    let temporal = data.map((employee: any) => {
      employee.TotalSalary = this.calculateTotalSalary(employee);
      employee['fecha'] = moment(`${employee.Year} ${employee.Month} 01 00 00 00`, `YYYY MM DD HH mm ss`).toISOString()
      return employee
    })
    temporal = this.sortByString(temporal, 'fecha').reverse();
    let consecutive: any[] = []
    for (let step = 3; step >= 2; step--) {
      if (consecutive.length === 0) {
        let pasos = temporal.length - step + 1
        for (let i = 0; i < pasos; i++) {
          let diferencia = moment(temporal[i].fecha).diff(temporal[i + step - 1].fecha, 'month');
          if (diferencia + 1 === step && consecutive.length === 0) {
            consecutive = temporal.slice(i, i + step)
          }
        }
      }
    }
    if (consecutive.length === 0) { consecutive = [temporal[0]] }
    return consecutive
  }

}
