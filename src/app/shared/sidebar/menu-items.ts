import { RouteInfo } from "./sidebar.metadata";

export const ROUTES: RouteInfo[] = [
  {
    path: "",
    title: "Management",
    icon: "mdi-dots-horizontal",
    class: "nav-small-cap",
    extralink: true,
    submenu: [],
  },
  {
    path: "/component/tabla-empleados",
    title: "Employee details",
    icon: "mdi mdi-account-card-details",
    class: "",
    extralink: false,
    submenu: [],
  },
  {
    path: "/component/agregar-empleado",
    title: "Add employee",
    icon: "mdi mdi-account-plus",
    class: "",
    extralink: false,
    submenu: [],
  },
  {
    path: "/component/promedio-salarios",
    title: "Average Salary",
    icon: "mdi mdi-poll",
    class: "",
    extralink: false,
    submenu: [],
  }
];
